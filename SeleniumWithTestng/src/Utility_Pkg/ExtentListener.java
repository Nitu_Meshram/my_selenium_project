package Utility_Pkg;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListener implements ITestListener {
	ExtentSparkReporter sparkreporter;
	ExtentReports report;
	ExtentTest test;

	public void reportconfiguration() {
		sparkreporter = new ExtentSparkReporter(".\\Extent-Report\\report.html");
		report = new ExtentReports();
		report.attachReporter(sparkreporter);
		report.setSystemInfo("OS", "Window 10");
		report.setSystemInfo("USER", "Nitu");
		sparkreporter.config().setDocumentTitle("selenium with testng extent report");
		sparkreporter.config().setReportName("My first report");
		sparkreporter.config().setTheme(Theme.DARK);
	}

	@Override
	public void onStart(ITestContext context) {
		reportconfiguration();
	}

	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("On start method ivoked" + "\n");

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		test = report.createTest(result.getName());
		test.log(Status.PASS,
				MarkupHelper.createLabel("Name of the passed method is " + result.getName(), ExtentColor.GREEN) + "\n");
		System.out.println(result.getName() + " Test method passed successfully");

	}

	@Override
	public void onTestFailure(ITestResult result) {
		test = report.createTest(result.getName());
		test.log(Status.PASS,
				MarkupHelper.createLabel("Name of the failed method is " + result.getName(), ExtentColor.RED) + "\n");
		System.out.println(result.getName() + " Test method failed");

	}

	@Override
	public void onTestSkipped(ITestResult result) {
		test = report.createTest(result.getName());
		test.log(Status.PASS,
				MarkupHelper.createLabel("Name of the skipped method is " + result.getName(), ExtentColor.AMBER)
						+ "\n");
		System.out.println(result.getName() + " Test method skipped successfully");

	}

	@Override
	public void onFinish(ITestContext context) {
		report.flush();
	}
}
