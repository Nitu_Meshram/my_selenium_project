package SeleniumIntegratedWithTestNg;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Commonfunction.InstantiateBrowser;

public class MSRTCpage {
	static WebDriver driver;

	@BeforeMethod
	public static void launchBrowser() {
		driver = InstantiateBrowser.instantiateBrowser("Chrome");
		InstantiateBrowser.launchURL(driver, "https://msrtc.maharashtra.gov.in/");
	}

	@Test(description = ":::::::::::::MSRTC Test Cases:::::::::::::::", priority = 3)
	public static void MSRTC_TC() {

		WebDriverWait expwait = new WebDriverWait(driver, Duration.ofSeconds(5));
//			WebElement language=expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@class=\"language\"]/li/a[contains(text(),\"English\")]")));
//			language.click();
		WebElement From = expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"from_txt\"]")));
		From.sendKeys("Nagpur");
		WebElement To = expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"to_txt\"]")));
		To.sendKeys("Pune");
		WebElement Date = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"journeydate\"]")));
		Date.click();
//			WebElement curDate=expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(@class,\"datepick-today\")]")));
//			curDate.click();

		List<WebElement> list = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//td[contains(@class, \"datepick-today\") or contains(@class,\"datepick-days-cell \" )]")));
		int count = list.size();
		for (int i = 0; i < count; i++) {
			String text = list.get(i).getText();
			System.out.println(text);
			if (text.contains("18")) {
				list.get(i).click();
				break;
			} else {
				System.out.println("Desired date is not found in current iteration : " + i + " , Hence retrying");
			}
		}

		WebElement submit = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type=\"submit\"])[2]")));
		submit.click();
	}

	@AfterMethod
	public static void quitDriver() {
		driver.quit();
	}

}
