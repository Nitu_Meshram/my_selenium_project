package SeleniumIntegratedWithTestNg;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Commonfunction.InstantiateBrowser;

public class SpiceJet {
	static WebDriver driver;

	@BeforeMethod
	public static void launchBrowser() {
		driver = InstantiateBrowser.instantiateBrowser("Chrome");
		InstantiateBrowser.launchURL(driver, "https://www.spicejet.com/");
	}

	@Test(description = "Spice jet Page automated ::::::::::::::::", priority = 4)
	public static void SpiceJet_TC() throws InterruptedException {
		WebDriverWait exp_wait = new WebDriverWait(driver, Duration.ofSeconds(7));
		Actions act = new Actions(driver);
		WebElement From = exp_wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//div[@data-testid=\"to-testID-origin\"]/div/div/input")));
		From.sendKeys("Nagpur");
		WebElement To = exp_wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//input[contains(@class,\"r-ubezar r-10paoce r-13qz1uu\")]")));
		List<WebElement> toList = exp_wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//div[contains(@class,\" r-1k1q3bj r-ql8eny r-1dqxon3\")]/div")));
		List<WebElement> toText = exp_wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("(//div[@style=\"margin-bottom: 1px;\"])[4]/div[contains(text(),\"Dehradun\")]")));
		int count = toList.size();
		for (int i = 0; i < count; i++) {
			String text = toText.get(i).getText();
			System.out.println(text);
			if (text.contains("Dehradun")) {
				toList.get(i + 3).click();
				break;
			} else {
				System.out.println("Element not found at given index hence retrying");
			}

		}

		List<WebElement> departlist = exp_wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
				"(//div[@class=\"css-1dbjc4n r-1mdbw0j r-ymttw5 r-b2dl2 r-mzjzbw r-wk8lta r-tvv088\"])[1]/div[3]/div/div[contains(@class,\"r-eu3ka r-1otgn73 r-1aockid\")]")));
		int count1 = departlist.size();
		for (int i = 0; i < count1; i++) {
			String text1 = departlist.get(i).getText();
			System.out.println(text1);
			if (text1.contains("20")) {
				departlist.get(i).click();
				break;
			} else {
				System.out.println("Element not found at given index hence retrying");
			}

		}

		WebElement returndate = exp_wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),\"Return Date\")]")));
		returndate.click();
		List<WebElement> returnlist = exp_wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//div[contains(text(),\"February \")]/../../div[3]/div/div")));
		int count2 = returnlist.size();
		for (int i = 0; i < count2; i++) {
			String text1 = returnlist.get(i).getText();
			System.out.println(text1);
			if (text1.contains("2")) {
				returnlist.get(i).click();
				break;
			} else {
				System.out.println("Element not found at given index hence retrying");
			}
		}
		WebElement selectPassengerNo = exp_wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),\"1 Adult\")]")));
		selectPassengerNo.click();
		WebElement incrementNo = exp_wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//div[contains(text(),\"more than 12 years\")]/../../div[2]/div[3]/*")));
		incrementNo.click();
		WebElement clickOutside = exp_wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//div[contains(@class, \"r-1uwte3a r-m611by r-bnwqim\")]")));
		clickOutside.click();

		WebElement selectOption = exp_wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("(//div[contains(text(),\"Govt. Employee\")])[2]")));
		selectOption.click();

		WebElement searchFlight = exp_wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//div[contains(text(),\"Search Flight\") and @class=\"css-76zvg2 r-jwli3a r-1i10wst r-1kfrs79\"]")));
		act.moveToElement(searchFlight).click().build().perform();
		WebElement agreeBox = exp_wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//*[local-name()=\"rect\" and @height=\"100%\"]")));
		agreeBox.click();
		WebElement continueClick = exp_wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//div[contains(@class,\" r-1loqt21 r-18u37iz r-1777fci r-d9fdf6 \")]")));
		continueClick.click();
	}

	@AfterMethod
	public static void quitDriver() { 
	
		driver.quit(); 
	}

}
