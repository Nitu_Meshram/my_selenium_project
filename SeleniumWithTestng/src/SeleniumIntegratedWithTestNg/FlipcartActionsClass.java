package SeleniumIntegratedWithTestNg;

import java.time.Duration;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Commonfunction.InstantiateBrowser;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class FlipcartActionsClass {

	static WebDriver Driver;
	static WebDriver driver;

	@BeforeMethod
	public static void beforeMethod() {
		Driver = InstantiateBrowser.instantiateBrowser("Chrome");
		InstantiateBrowser.launchURL(Driver, "https://www.flipkart.com/");
	}

	@Test(description = ":::::::::::::MSRTC Test Cases:::::::::::::::", priority = 3)
	public static void MSRTC_TC() throws InterruptedException {
		WebDriverWait expwait = new WebDriverWait(Driver, Duration.ofSeconds(5));
		Actions act = new Actions(Driver);
		WebElement doubleclick = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[@class=\"_2o9o_t\"])[1]")));
		act.doubleClick(doubleclick).build().perform();
		// act.moveToElement(doubleclick).doubleClick().build().perform();

		WebElement rightclick = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()=\"Login\"]")));
		act.contextClick(rightclick).build().perform();
		// act.moveToElement(rightclick).contextClick().build().perform();

		WebElement upperCase = expwait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//input[contains(@title,\"Products, Brands \")]")));
		act.moveToElement(upperCase).click().keyDown(Keys.SHIFT).sendKeys("shirt").keyUp(Keys.SHIFT).keyDown(Keys.ENTER)
				.build().perform();

		driver = InstantiateBrowser.instantiateBrowser("chrome");
		InstantiateBrowser.launchURL(driver, "https://www.globalsqa.com/demo-site/draganddrop/#Photo%20Manager");
		WebDriverWait expwait1 = new WebDriverWait(driver, Duration.ofSeconds(5));
		Actions act1 = new Actions(driver);
		WebElement frame = expwait1
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//iframe[@class=\"demo-frame lazyloaded\"]")));
		driver.switchTo().frame(frame);
		WebElement source = expwait1
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt=\"The peaks of High Tatras\"]")));
		WebElement target = expwait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id=\"trash\"]")));
		act1.dragAndDrop(source, target).build().perform();
		driver.switchTo().parentFrame();
		// driver.switchTo().defaultContent();

	}
	@AfterMethod
	public static void quitDriver() {
		driver.quit();
		Driver.quit();
	}
		
}
