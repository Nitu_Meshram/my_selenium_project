package TestCases;

import java.time.Duration;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Commonfunction.CommonMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class FlipcartActionsClass {
		public static void main(String[] args) throws InterruptedException {
		WebDriver Driver = CommonMethods.instantiateBrowser("Chrome");
		CommonMethods.launchURL(Driver, "https://www.flipkart.com/");
		WebDriverWait expwait = new WebDriverWait(Driver, Duration.ofSeconds(5));
		Actions act = new Actions(Driver);
		WebElement doubleclick = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[@class=\"_2o9o_t\"])[1]")));
		act.doubleClick(doubleclick).build().perform();
		// act.moveToElement(doubleclick).doubleClick().build().perform();

		WebElement rightclick = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()=\"Login\"]")));
		act.contextClick(rightclick).build().perform();
		// act.moveToElement(rightclick).contextClick().build().perform();

		WebElement upperCase = expwait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//input[contains(@title,\"Products, Brands \")]")));
		act.moveToElement(upperCase).click().keyDown(Keys.SHIFT).sendKeys("shirt").keyUp(Keys.SHIFT).keyDown(Keys.ENTER)
				.build().perform();
		Thread.sleep(3000);
		Driver.quit();

		WebDriver driver = CommonMethods.instantiateBrowser("chrome");
		CommonMethods.launchURL(driver, "https://www.globalsqa.com/demo-site/draganddrop/#Photo%20Manager");
		WebDriverWait expwait1 = new WebDriverWait(driver, Duration.ofSeconds(5));
		Actions act1 = new Actions(driver);
		WebElement frame = expwait1
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//iframe[@class=\"demo-frame lazyloaded\"]")));
		driver.switchTo().frame(frame);
		WebElement source = expwait1
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt=\"The peaks of High Tatras\"]")));
		WebElement target = expwait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id=\"trash\"]")));
		act1.dragAndDrop(source, target).build().perform();
		driver.switchTo().parentFrame();
		// driver.switchTo().defaultContent();
		driver.quit();

	}

}
