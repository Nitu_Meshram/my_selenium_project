package TestCases;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Commonfunction.CommonMethods;

public class RedBusPageAutomation {

	public static void main(String[] args) {
		WebDriver Driver = CommonMethods.instantiateBrowser("chrome");
		CommonMethods.launchURL(Driver, "https://www.redbus.in/");
		WebDriverWait expwait = new WebDriverWait(Driver, Duration.ofSeconds(10));
		WebElement From = expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"src\"]")));
		From.sendKeys("nagpur");
		WebElement To = expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"dest\"]")));
		To.sendKeys("shirdi");

		List<WebElement> List_click = expwait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//ul[@class=\"sc-dnqmqq eFEVtU\"]")));
		List<WebElement> List_text = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//ul[@class=\"sc-dnqmqq eFEVtU\"]/li/div/text[contains(text(),\"Police Chowki\")]")));
		int count = List_click.size();
		for (int i = 0; i < count; i++) {
			String text = List_text.get(i).getText();
			System.out.println(text);
			if (text.contains("Chowki")) {
				List_click.get(i).click();
				break;
			} else {
				System.out.println("Element not found at " + i + "th index hence retrying");
			}
		}

		WebElement date = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@class=\"sc-cSHVUG NyvQv icon icon-datev2\"]")));
		date.click();
		List<WebElement> dateList = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
				"//div[contains(@class,\"DatePicker__MainBlock\")]/div[3]/div/span/div/span[contains(@class, \"fgdqFw\") or contains(@class,\" bwoYtA\") or contains(@class, \" dkWAbH\")]")));
		
		int count1 = dateList.size();
		for (int i = 0; i < count1; i++) {
			String text1 = dateList.get(i).getText();
			System.out.println(text1);
			if (text1.contains("20")) {
				dateList.get(i).click();
				break;
			} else {
				System.out.println("Element not found at " + i + " the index hence retrying");
			}
		}
		Driver.quit();

	}

}
