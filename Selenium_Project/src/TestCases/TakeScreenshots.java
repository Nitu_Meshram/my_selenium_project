package TestCases;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import Commonfunction.CommonMethods;

public class TakeScreenshots {
	
	public static void main(String[] args) throws IOException {
		WebDriver Driver=CommonMethods.instantiateBrowser("chrome");
		CommonMethods.launchURL(Driver, "https://gitlab.com/Nitu_Meshram");
		CommonMethods.takeScreenshot(Driver, "My_Git_Homepage");
	   
	    
	    CommonMethods.launchURL(Driver, "https://www.google.com/");
		CommonMethods.takeScreenshot(Driver, "Google_Homepage");
	   
		CommonMethods.launchURL(Driver, "https://reqres.in/");
		CommonMethods.takeScreenshot(Driver, "ReqRes_Homepage");
		
		CommonMethods.launchURL(Driver, "https://www.amazon.in/");
		CommonMethods.takeScreenshot(Driver, "Amazon_Homespage");
		
		Driver.quit();
		
	    
	}

}
