package TestCases;

import java.time.Duration;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import Commonfunction.CommonMethods;

public class WindowScrollOperation {

	public static void main(String[] args)  {
		WebDriver Driver=CommonMethods.instantiateBrowser("chrome");
		CommonMethods.launchURL(Driver, "https://www.amazon.in/");
		CommonMethods.windowScroll(Driver); 
	
		Driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		try {
			Thread.sleep(2000)	;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		Driver.quit();
		
//		WebDriver Driver1=CommonMethods.instantiateBrowser("chrome");
//		CommonMethods.launchURL(Driver1, "https://www.amazon.in/");
//		Actions act=new Actions(Driver1);
//		act.sendKeys(Keys.PAGE_DOWN).build().perform();
//		try {
//			Thread.sleep(2000);
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//		}
//		act.sendKeys(Keys.PAGE_UP).build().perform();
//		
//		Driver1.quit();

	}

}
