package TestCases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Commonfunction.CommonMethods;

public class DyanamicDropdown {

	public static void main(String[] args) throws InterruptedException {
		WebDriver Driver = CommonMethods.instantiateBrowser("Chrome");
		CommonMethods.launchURL(Driver, "https://www.google.co.in/");

		// Fetch the webelement of dropdown
		WebElement searchbox = Driver.findElement(By.xpath("//textarea[@aria-label=\"Search\"]"));

		// Insert text
		searchbox.sendKeys("selenium");

		// Wait for 3 seconds so that list is visible on screen
		Thread.sleep(3000);

		// Fetch the options webelement
		List<WebElement> options_click = Driver.findElements(By.xpath("(//ul[@role=\"listbox\"])[1]/li"));
		List<WebElement> options_text = Driver
				.findElements(By.xpath("(//ul[@role=\"listbox\"])[1]/li/div/div[2]/div[1]/div[1]/span/b"));

		// Fetch the count of options webelements
		int count = options_click.size();

		for (int i = 0; i < count - 1; i++) {
			String option_value = options_text.get(i).getText();
			System.out.println(option_value);
			if (option_value.equals("dev")) {
				options_click.get(i + 1).click();
				break;
			} else {
				System.out.println("Desired option not found after checking " + i + "th option");
			}

		}

		Driver.quit();

///////////////////////:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::://///////////
		WebDriver driver = CommonMethods.instantiateBrowser("Chrome");
		CommonMethods.launchURL(driver, "https://www.google.co.in/");
		WebElement element = driver.findElement(By.xpath("//textarea[@id=\"APjFqb\"]"));
		element.sendKeys("globalprotect");
		Thread.sleep(2000);
		
		List<WebElement> input_click = driver.findElements(By.xpath("(//ul[@class=\"G43f7e\"])[1]/li"));
		List<WebElement> input_text = driver
				.findElements(By.xpath("(//ul[@class=\"G43f7e\"])[1]/li/div[1]/div[2]/div[1]/div/span/b"));
		
		int count1 = input_click.size();
		for (int i = 0; i < count1 - 1; i++) {
			String input_value = input_text.get(i).getText();
			System.out.println(input_value);
			if (input_value.equals("download")) {
				input_click.get(i + 1).click();
				break;
			} else {
				System.out.println("Desired option not found after checking " + i + "th option");
			}
		}
		driver.quit();
	}

}
