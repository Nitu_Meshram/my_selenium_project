package TestCases;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Commonfunction.CommonMethods;

public class MSRTCpage {

	
	public static void main(String[] args) {
		WebDriver driver = CommonMethods.instantiateBrowser("Chrome");
		CommonMethods.launchURL(driver, "https://msrtc.maharashtra.gov.in/");
		WebDriverWait expwait = new WebDriverWait(driver, Duration.ofSeconds(5));
//			WebElement language=expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@class=\"language\"]/li/a[contains(text(),\"English\")]")));
//			language.click();
		WebElement From = expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"from_txt\"]")));
		From.sendKeys("Nagpur");
		WebElement To = expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"to_txt\"]")));
		To.sendKeys("Pune");
		WebElement Date = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"journeydate\"]")));
		Date.click();
//			WebElement curDate=expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(@class,\"datepick-today\")]")));
//			curDate.click();

		List<WebElement> list = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//table[@class=\"datepick\"]/tbody/tr/td[contains(@class,\"datepick-today\") or contains(@class,\"datepick-days-cell\")]")));
		int count = list.size();
		for (int i = 0; i < count; i++) {
			String text = list.get(i).getText();
			System.out.println(text);
			if (text.contains("18")) {
				list.get(i).click();
				break;
			} else {
				System.out.println("Desired date is not found in current iteration : " + i + " , Hence retrying");
			}
		}

		WebElement submit = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type=\"submit\"])[2]")));
		submit.click();
		driver.quit();

	}

}
