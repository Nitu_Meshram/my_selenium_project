package TestCases;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import Commonfunction.CommonMethods;

public class StaticDropdown {

	public static void main(String[] args) {
		WebDriver driver = CommonMethods.instantiateBrowser("Chrome");
		CommonMethods.launchURL(driver, "https://www.amazon.in/");

		// Find the dropdown webelement using the xpath of dropdown to be automated
		WebElement dropdown = driver.findElement(By.cssSelector("#searchDropdownBox"));
		List<WebElement> elements = driver.findElements(By.xpath("//select[@id=\"searchDropdownBox\"]//option"));

		// Create and object of select class with argument of dropdown webelement.
		Select S1 = new Select(dropdown);
		// select By index, By Value or By visible text
		S1.selectByIndex(15);
		S1.selectByValue("search-alias=jewelry");
		S1.selectByVisibleText("Musical Instruments");

		// Selecting multiple options from the drop down
		int count = elements.size();
		for (int i = 0; i < count; i++) {
			S1.selectByIndex(i);
		}
		driver.quit();


	}

}
