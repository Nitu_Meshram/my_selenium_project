package TestCases;

import org.openqa.selenium.WebDriver;

import Commonfunction.CommonMethods;

public class CrossBrowserTesting {

	public static void main(String[] args) {
		// Can do cross browser testing
				WebDriver chromeDriver=CommonMethods.instantiateBrowser("Chrome");
				chromeDriver.get("https://www.flipkart.com/");
				chromeDriver.manage().window().maximize();
				chromeDriver.quit();
				
				WebDriver firefoxDriver=CommonMethods.instantiateBrowser("Firefox");
				firefoxDriver.get("https://www.flipkart.com/");
				firefoxDriver.quit();
				
				WebDriver edgeDriver=CommonMethods.instantiateBrowser("Edge");
				edgeDriver.get("https://www.flipkart.com/");
				edgeDriver.quit();



	}

}
