package Commonfunction;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.JavascriptExecutor;

import com.google.common.io.Files;

public class CommonMethods {
	public static WebDriver instantiateBrowser(String Browser) {
		WebDriver driver = null;
		switch (Browser) {
		case "Chrome":
			driver = new ChromeDriver();
			break;
		case "Firefox":
			driver = new FirefoxDriver();
			break;
		case "Edge":
			driver = new EdgeDriver();
			break;
		default:
			driver = new ChromeDriver();
		}
		return driver;
	}

	public static void launchURL(WebDriver driver, String URL) {
		driver.get(URL);
		driver.manage().window().maximize();
		driver.navigate().refresh();
	}

	public static void takeScreenshot(WebDriver driver, String ScreenshotName) {
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File filename = new File(".\\Screenshots\\" + ScreenshotName + ".png");
		try {
			Files.copy(screenshot, filename);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void windowScroll(WebDriver driver) {
//scrollBy(x-pixels,y-pixels)"); x-pixels is the number at x-axis, it moves to the left if number is positive
//and it move to the right if number is negative . y-pixels is the number at y-axis, it moves to the down if number is positive 
//and it move to the up if number is in negative .
//scrollTo method check current scrolled position and compares with (0,0) and decides whether to scroll or not. (0,500) fir (0,800) karna hai to only 300 pixel bdhega		
//scrollBy consider current position as (0,0) and scroll further. scroll method same as scrollTo
		
		JavascriptExecutor JS = (JavascriptExecutor) driver;
		//vertical scroll
		JS.executeScript("window.scrollBy(0,500)");
	 	JS.executeScript("window.scrollBy(0,-1000)");
				
		// scroll down by 1000 pixcels vertical
//		JS.executeScript("window.scrollBy(0,1000)");
//		
		// scroll to the bottom of page
//		JS.executeScript("window.scrollTo(0,document.body.scrollHeight,0)");
		
		//scroll till the element is visible and use for horizontal scroll
//      js.executeScript("arguments[0].scrollIntoView();", Element);
	}

}
