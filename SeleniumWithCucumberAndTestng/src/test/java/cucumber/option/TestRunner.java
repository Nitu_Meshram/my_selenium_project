package cucumber.option;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions (features="src/test/java/Feature.feature", glue= {"stepDefinitions"},tags="@IRCTC")
public class TestRunner {

}
