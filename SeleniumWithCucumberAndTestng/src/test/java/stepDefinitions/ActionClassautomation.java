package stepDefinitions;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import instantiateBrowser.CommonFunction;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ActionClassautomation {
	WebDriver Driver;
	WebDriverWait expwait;
	Actions act;
	WebDriver driver;
	WebDriverWait expwait1;
	Actions act1;
@Test(priority=1)
	@Given("flipcart application to test, launch browser")
	public void flipcart_application_to_test() {
		Driver = CommonFunction.instantiateBrowser("Chrome");

	}
@Test(priority=2)
	@When("launch the URL of flipcart application")
	public void launch_flipcart_application() {
		CommonFunction.launchURL(Driver, "https://www.flipkart.com/");
		expwait = new WebDriverWait(Driver, Duration.ofSeconds(5));
		act = new Actions(Driver);
	}
@Test(priority=3)
	@Then("automating right click, double click and upper case option")
	public void automating_right_click_double_click_and_upper_case_option() throws InterruptedException {
		WebElement doubleclick = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[@class=\"_2o9o_t\"])[1]")));
		act.doubleClick(doubleclick).build().perform();
		// act.moveToElement(doubleclick).doubleClick().build().perform();

		WebElement rightclick = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()=\"Login\"]")));
		act.contextClick(rightclick).build().perform();
		// act.moveToElement(rightclick).contextClick().build().perform();

		WebElement upperCase = expwait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//input[contains(@title,\"Products, Brands \")]")));
		act.moveToElement(upperCase).click().keyDown(Keys.SHIFT).sendKeys("shirt").keyUp(Keys.SHIFT).keyDown(Keys.ENTER)
				.build().perform();
		Thread.sleep(3000);
		Driver.quit();
	}
@Test(priority=4)
	@Then("launch another application globalsqa")
	public void launch_another_application_globalsqa() {
		driver = CommonFunction.instantiateBrowser("chrome");
		CommonFunction.launchURL(driver, "https://www.globalsqa.com/demo-site/draganddrop/#Photo%20Manager");
		expwait1 = new WebDriverWait(driver, Duration.ofSeconds(5));
		act1 = new Actions(driver);
	}
@Test(priority=5)
	@Then("automated drag and drop feature")
	public void automated_drag_and_drop_feature() {
		WebElement frame = expwait1
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//iframe[@class=\"demo-frame lazyloaded\"]")));
		driver.switchTo().frame(frame);
		WebElement source = expwait1
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt=\"The peaks of High Tatras\"]")));
		WebElement target = expwait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id=\"trash\"]")));
		act1.dragAndDrop(source, target).build().perform();
		driver.switchTo().parentFrame();
		driver.quit();
	}

}
