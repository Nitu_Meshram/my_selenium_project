package stepDefinitions;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import instantiateBrowser.CommonFunction;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MSRTC_Automation {

	WebDriver driver;
	WebDriverWait expwait;

	@Test(priority=11)
	@Given("MSRTC application to test, launch browser")
	public void msrtc_application_to_test_launch_browser() {
		driver = CommonFunction.instantiateBrowser("Chrome");
		expwait = new WebDriverWait(driver, Duration.ofSeconds(5));
	}
    
	@Test(priority=12)
	@When("launch the URL of MSRTC application")
	public void launch_the_url_of_msrtc_application() {
		CommonFunction.launchURL(driver, "https://msrtc.maharashtra.gov.in/");
	}

	@Test(priority=13)
	@Then("automating To, From, and date web element options")
	public void automating_to_from_and_date_web_element_options() {
		WebElement From = expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"from_txt\"]")));
		From.sendKeys("Nagpur");
		WebElement To = expwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"to_txt\"]")));
		To.sendKeys("Pune");
		WebElement Date = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id=\"journeydate\"]")));
		Date.click();

		List<WebElement> list = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//table[@class=\"datepick\"]/tbody/tr/td[@class=\"datepick-days-cell \"]")));
		int count = list.size();
		for (int i = 0; i < count; i++) {
			String text = list.get(i).getText();
			System.out.println(text);
			if (text.contains("16")) {
				list.get(i).click();
				break;
			} else {
				System.out.println("Desired date is not found in current iteration : " + i + " , Hence retrying");
			}
		}

		WebElement submit = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type=\"submit\"])[2]")));
		submit.click();
	}
	@Test(priority=14)
	@Then("quiting the MSRTC browser")
	public void quiting_the_msrtc_browser() {
		driver.quit();
	}
}
