package stepDefinitions;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import instantiateBrowser.CommonFunction;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class IRCTC_Automation {
	WebDriver driver;
	WebDriverWait expwait;
	
	@Test(priority=6)
	@Given("IRCTC application to test, launch browser")
	public void irctc_application_to_test_launch_browser() {
		driver = CommonFunction.instantiateBrowser("Chrome");
	}
	@Test(priority=7)
	@When("launch the URL of IRCTC application")
	public void launch_the_url_of_irctc_application() {
		CommonFunction.launchURL(driver, "https://www.irctc.co.in/nget/train-search");
		expwait = new WebDriverWait(driver, Duration.ofSeconds(7));
		}
	@Test(priority=8)
	@Then("automating To, From, Select coach, select class options")
	public void automating_to_from_select_coach_select_class_options() throws InterruptedException {
		WebElement from = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@aria-controls=\"pr_id_1_list\"]")));
		from.sendKeys("Mumbai");
		List<WebElement> fromclick = expwait
				.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//ul[@id=\"pr_id_1_list\"]/li"))); 
																														
																														
		List<WebElement> fromtext = expwait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//ul[@id=\"pr_id_1_list\"]/li[3]/span")));
																															
																															
		int count = fromclick.size();
		for (int i = 0; i < count; i++) {
			String text1 = fromtext.get(i).getText();
			System.out.println(text1);
			if (text1.contains("CENTRAL - BCT")) {
				fromclick.get(i + 2).click();
				break;
			} else {
				System.out.println("desired ouput not found at " + i + " iteration hence retrying");
			}
		}
		WebElement general = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class,\"ng-tns-c65-12 \")]")));
		general.click();
		List<WebElement> general_click = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//ul[contains(@class, \" ui-dropdown-list ui-widget-content \")]/p-dropdownitem")));
		List<WebElement> general_text = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By
				.xpath("//ul[contains(@class, \" ui-dropdown-list ui-widget-content \")]/p-dropdownitem[6]/li/span")));

		int count5 = general_click.size();
		for (int i = 0; i < count5; i++) {
			String text5 = general_text.get(i).getText();
			System.out.println(text5);
			if (text5.contains("TATKAL")) {
				general_click.get(i + 5).click();
				break;
			} else {
				System.out.println("expected result not found at " + i + "th index hence retrying ");
			}

		}

		WebElement date = expwait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//input[contains(@class,\"ui-state-default ui-corner-all ng-star-inserted\")]")));
		date.click();
		List<WebElement> datelist = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//table[contains(@class,\"ui-datepicker-calendar\")]/tbody/tr/td/a")));
		int count2 = datelist.size();
		for (int i = 0; i < count2; i++) {
			String text = datelist.get(i).getText();
			System.out.println(text);
			if (text.contains("17")) {
				datelist.get(i).click();
				break;
			} else {
				System.out.println("desired ouput not found at " + i + " iteration hence retrying");
			}
		}

		WebElement to = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@aria-controls=\"pr_id_2_list\"]")));
		to.sendKeys("Nagpur");
		List<WebElement> toclick = expwait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.xpath("//ul[@role=\"listbox\" and @id=\"pr_id_2_list\"]/li")));
		List<WebElement> totext = expwait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//ul[@id=\"pr_id_2_list\"]/li[3]/span")));
		int count1 = toclick.size();
		for (int i = 0; i < count1; i++) {
			String text2 = totext.get(i).getText();
			System.out.println(text2);
			if (text2.contains("AJNI - AJNI")) {
				toclick.get(i + 2).click();
				break;
			} else {
				System.out.println("desired ouput not found at " + i + " iteration hence retrying");
			}
		}

		WebElement selectclass = expwait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//span[@class=\"ng-tns-c65-11 ui-dropdown-label ui-inputtext ui-corner-all ng-star-inserted\"]")));
		selectclass.click();
		List<WebElement> class_click = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//div[@class=\"ui-dropdown-items-wrapper ng-tns-c65-11\"]/ul/p-dropdownitem")));
		List<WebElement> class_text = expwait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.xpath("//div[@class=\"ui-dropdown-items-wrapper ng-tns-c65-11\"]/ul/p-dropdownitem[10]/li/span"))); //// div[contains(@class,\"ui-dropdown-items-wrapper
																														//// ng-tns-c65-11\")]/ul/p-dropdownitem[2]/li/span

		int count4 = class_click.size();
		for (int i = 0; i < count4; i++) {
			String text4 = class_text.get(i).getText();
			System.out.println(text4);
			if (text4.contains("Vistadome")) {
				class_click.get(i + 1).click();
				break;
			} else {
				System.out.println("desired ouput not found at " + i + " iteration hence retrying");
			}
		}
		WebElement options = expwait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("(//div[@class=\"col-xs-12 remove-padding\"])[2]/span[3]")));
		options.click();
		WebElement searchbox = expwait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(), \"Search\")]")));
		searchbox.click();
		Thread.sleep(3000);
	}
	@Test(priority=9)
	@Then("quiting the browser")
	public void quiting_the_browser() {
		driver.navigate().back();
		driver.quit();
	}




}
