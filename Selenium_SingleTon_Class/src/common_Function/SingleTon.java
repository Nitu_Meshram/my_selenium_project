package common_Function;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SingleTon {
//singleton class can have only one object (instance) of class at a time
//make a constructor as private
//write public static method (getInstance) that has return type of object of this singleton class(lazy initialization)
//for normal class we use constructor where as in singleton we use getInstance method

	private static SingleTon driverInstance = null;

	private SingleTon() {

	}

	public static SingleTon getInstance() {
		if (driverInstance == null) {
			driverInstance = new SingleTon();
		}
		return driverInstance;
	}
	
	public WebDriver commonMethod() {
		WebDriver driver=new ChromeDriver();
		return driver;
	}

}
