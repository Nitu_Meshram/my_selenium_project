package pageTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common_Function.SingleTon;

public class facebookLogin {
	WebDriver driver;// =new ChromeDriver();

	@BeforeMethod
	public void setUp() {
		SingleTon driverInstance = SingleTon.getInstance();
	   driver=driverInstance.commonMethod();

	}

	@Test
	public void testMethod() {
		driver.get("https://www.facebook.com/login/");
		driver.manage().window().maximize();
		System.out.println(driver.getTitle());
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
