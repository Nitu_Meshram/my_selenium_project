# My_Selenium_Project
My selenium project Consists of component like Common function and Test Cases I automated using Selenium-Java.

Common function: --> It consists of method to instantiate the browser using WebDriver and launch different browsers as per the requirement. This method can also be used to do cross browser testing.

Test cases: --> 
Static Dropdown:--> By using selenium along with java, I have automated the static dropdown. For static dropdown used select class predefined methods like ByIndex, ByValue, and ByVisibleText. In case select tab not available in html page can be automated by comparing expected result and actual result.

Dynamic dropdown:--> For this firstly located the list of all the element present in the dropdown with respect to the input provided and then reaching the required text by creating relative path for the element. Then traverse through the list using for loop and compare actual result with expected result. If the result are matching then click on the expected input else try again till desired output not found.

Calendar:--> Automated the calendar using the same method used in dynamic dropdown, and also used the implicit wait here to avoid wastage of time due to stoppage of execution that occur while using thread.sleep method.

Auto Suggestive Dropdown with explicit wait: --> Implemented explicit wait to the dynamic dropdown I have automated. Wastage of time and halting of execution is removed and also the wait has been specific to the web element I am automating. Along with this specified the condition of wait such as elementToBe Clickable,VisibilityOfElementsLocatedBy etc.

Actions class:--> It used to automate the keyboard and mouse operation, Like doing double click, right click, drag and drop, sending input in Upper Case etc.

Selenium And Testng: --> Added testng dependency to the pom.xml, created test cases with @Test annotation, define priority of execution and executed test cases with the help of testng.xml. Generated extent report of execution as well.

Selenium with Cucumber: --> Added cucumber-java, cucumber-junit dependency to pom.xml and created feature file defining the application under test and steps to automate the application. Created step definition file to map feature steps with java code and executed test cases with the help of Test Runner class.

Selenium with Testng and cucumber: --> Created feature file, step definition file. Added @Test annotation to the test cases under test and executed them with help of testng.xml or can execute using runner class. 
