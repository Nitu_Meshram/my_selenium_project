package pageClass;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RedBusLoginPage {
	WebDriver driver = null;
	

	public RedBusLoginPage(WebDriver driver1) {
		this.driver = driver1;
		WebDriverWait expwait = new WebDriverWait(driver, Duration.ofSeconds(10));
		PageFactory.initElements(driver1, this);
	}

//	@FindBy(xpath = "(//text[@class=\"placeHolderMainText\"])[1]")
//	WebElement fromTab;
//	@FindBy(xpath = "(//text[@class=\"placeHolderMainText\"])[2]")
//	WebElement toTab;
	@FindBy(xpath = "//i[contains(@class, \"icon icon-datev2\")]")
	WebElement dateTab;

	@FindBy(xpath = "//div[contains(@class, \"DatePicker__MainBlock\")]/div[3]/div/span/div/span[contains(@class, \"fgdqFw\") or contains(@class,\"dkWAbH\") or contains(@class,\"bwoYtA\")]")
	List<WebElement> dateClick;

	public void ActionOnWebelement() {
	
		this.dateTab.click();
		int count = dateClick.size();
		for (int i = 0; i < count; i++) {
			String text = dateClick.get(i).getText();
			if (text.contains("28")) {
				dateClick.get(i).click();
			}

		}

	}
}
