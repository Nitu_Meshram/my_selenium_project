package pageClass;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class FaceBookLoginPage {
	WebDriver driver;
	
	public FaceBookLoginPage(WebDriver driver1) {
		this.driver = driver1;
		PageFactory.initElements(driver1, this);
	}
	
// @FindAll({@FindBy(xpath="")},{@FindBy(cssSelector=)})	
	@FindBy(xpath="//input[@id=\"email\"]") WebElement UserId;
	@CacheLookup // save element into cache memory and instead of finding element on web page every time look into cache memory
//	@FindBy(how=How.ID, using="email") WebElement UserId; //another way of using @findBy annotations
	@FindBy(xpath="//input[@id=\"pass\"]") WebElement PassWord;
	@FindBy(xpath="//button[@name=\"login\"]") WebElement loginTab;
	
	
	
	public void loginInfo(String UID,String PASS) {
		new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(UserId)).sendKeys(UID);
		new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(PassWord)).sendKeys(PASS);
		new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(loginTab)).click();
//		UserId.sendKeys(UID);
//		PassWord.sendKeys(PASS);
//		loginTab.click();
	}
	
		
}
