package common_Function;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CommonFunction {
	public static WebDriver browserLaunch(String browser, String url) {
		WebDriver driver=null;
		if (browser.equals("chrome")) {
			driver=new ChromeDriver();
		}
		else if (browser.equals("Firefox")) {
			driver=new FirefoxDriver();
		}
		else if (browser.equals("Edge")) {
			driver=new EdgeDriver();
		}
			
		driver.get(url);
		driver.manage().window().maximize();
		driver.navigate().refresh();
		return driver;
	}
	}
