package testClass;

import org.openqa.selenium.WebDriver;

import common_Function.CommonFunction;
import pageClass.RedBusLoginPage;

public class RedbusTest {

	public static void main(String[] args) {
		WebDriver driver = CommonFunction.browserLaunch("chrome", "https://www.redbus.in/");
		RedBusLoginPage redBus = new RedBusLoginPage(driver);
		redBus.ActionOnWebelement();
		driver.quit();
	}

}
