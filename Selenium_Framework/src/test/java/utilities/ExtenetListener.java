package utilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtenetListener implements ITestListener {
ExtentReports report;
ExtentSparkReporter sparkreporter;
ExtentTest test;
public void reportConfiguration() {
	report=new ExtentReports();
	sparkreporter=new ExtentSparkReporter(".\\Extent-report\\report.html");
	report.attachReporter(sparkreporter);
	report.setSystemInfo("OS", "Window 10");
	report.setSystemInfo("User", "User");
	sparkreporter.config().setDocumentTitle("My selenium Report");
	sparkreporter.config().setReportName("Facebook Login-page");
	sparkreporter.config().setTheme(Theme.DARK);
}

@Override
	public void onStart(ITestContext context) {
	reportConfiguration();
	System.out.println("OnStart method invoked  ");
	}
@Override
	public void onTestStart(ITestResult result) {
	System.out.println("OnTestStart method invoked  ");	
	}
@Override
	public void onTestSuccess(ITestResult result) {
	System.out.println("OnTestSuccess method invoked  ");
	test=report.createTest(result.getName());
	test.log(Status.PASS, MarkupHelper.createLabel("Successful test method named -->"+result.getName(), ExtentColor.GREEN));
	}
@Override
	public void onTestFailure(ITestResult result) {
	System.out.println("OnTestFailure method invoked  ");
	test=report.createTest(result.getName());
	test.log(Status.PASS, MarkupHelper.createLabel("Failed test method named -->"+result.getName(), ExtentColor.RED));
	}
@Override
	public void onTestSkipped(ITestResult result) {
	System.out.println("OnTestSkipped method invoked  ");
	test=report.createTest(result.getName());
	test.log(Status.PASS, MarkupHelper.createLabel("Skipped test method named -->"+result.getName(), ExtentColor.AMBER));
	}
@Override
	public void onFinish(ITestContext context) {
	System.out.println("OnFinish method invoked  ");	
	report.flush();
	}
}
