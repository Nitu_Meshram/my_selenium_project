package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.annotations.DataProvider;

public class excelDataExtractor {
	@DataProvider(name = "test")
	public String[][] readData(Method m) throws EncryptedDocumentException, IOException {
		
		String sheetName = m.getName();
	    File newFile=new File(".\\src\\test\\resources\\testData\\testData.xlsx");
		FileInputStream FIS = new FileInputStream(newFile);
		Workbook wb = WorkbookFactory.create(FIS);
		Sheet sheetname = wb.getSheet(sheetName);
		int totalRows = sheetname.getLastRowNum();
		System.out.println(totalRows);
		Row row = sheetname.getRow(0);
		int totalCols = row.getLastCellNum();
		System.out.println(totalCols);
		DataFormatter format = new DataFormatter();
		String[][] testData = new String[totalRows][totalCols];
		for (int i = 1; i <= totalRows; i++) {
			for (int j = 0; j < totalCols; j++) {
				testData[i-1][j] = format.formatCellValue(sheetname.getRow(i).getCell(j));
			
			}
		}
		return testData;
	}
}
