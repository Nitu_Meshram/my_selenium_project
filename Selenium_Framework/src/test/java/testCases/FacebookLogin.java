package testCases;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import Base.BaseTest;
import utilities.excelDataExtractor;

public class FacebookLogin extends BaseTest {
	@Test(dataProvider = "test", dataProviderClass=excelDataExtractor.class)
	public void login_tc(String userid, String pass) throws InterruptedException {
		WebDriverWait expwait = new WebDriverWait(driver, Duration.ofSeconds(15));
		expwait.until(ExpectedConditions.elementToBeClickable(By.xpath(loc.getProperty("userid")))).sendKeys(userid);
		Thread.sleep(2000);
		expwait.until(ExpectedConditions.elementToBeClickable(By.xpath(loc.getProperty("password")))).sendKeys(pass);
		Thread.sleep(2000);
		expwait.until(ExpectedConditions.elementToBeClickable(By.xpath(loc.getProperty("login")))).click();

	}

//	@DataProvider(name = "testData")
//	public Object[][] testData() {
//		return new Object[][] { { "xyz@gmail.com", "xyz123" }, { "abc@gmail.com", "abc1234" } };
//	}

}
