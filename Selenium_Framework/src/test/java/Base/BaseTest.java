package Base;

import java.io.FileReader;
import java.io.IOException;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


public class BaseTest {
	public WebDriver driver = null;
	public Properties prop = new Properties();
	public Properties loc = new Properties();
	public FileReader fr;
	public FileReader fr1;

	@BeforeMethod(description = "....Test setup done")
	public void setUp() throws IOException {
	
		if (driver == null) {
			fr = new FileReader(".\\src\\test\\resources\\configFiles\\config.properties");
			fr1 = new FileReader(".\\src\\test\\resources\\configFiles\\locators.properties");
			prop.load(fr);
			loc.load(fr1);
		}
		switch (prop.getProperty("Browser")) {
		case "Chrome":
			driver = new ChromeDriver();
			driver.get(prop.getProperty("TestUrl"));
			driver.manage().window().maximize();
			driver.navigate().refresh();
			break;
		case "Firefox":
			driver = new FirefoxDriver();
			driver.get(prop.getProperty("TestUrl"));
			driver.manage().window().maximize();
			driver.navigate().refresh();
			break;
		case "Edge":
			driver = new EdgeDriver();
			driver.get(prop.getProperty("TestUrl"));
			driver.manage().window().maximize();
			driver.navigate().refresh();
			break;
		default:
			driver = new ChromeDriver();
			driver.get(prop.getProperty("TestUrl"));
			driver.manage().window().maximize();
			driver.navigate().refresh();
					
		}
	}

	
	@AfterMethod(description = ".........Tear down successful")
	public void tearDown() {
		driver.quit();
	}
}
