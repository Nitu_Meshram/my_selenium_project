package instantiateBrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CommonFunction {
	public static WebDriver instantiateBrowser(String Browser) {
		WebDriver driver = null;
		switch (Browser) {
		case "Chrome":
			driver = new ChromeDriver();
			break;
		case "Firefox":
			driver = new FirefoxDriver();
			break;
		case "Edge":
			driver = new EdgeDriver();
			break;
		default:
			driver = new ChromeDriver();
		}
		return driver;
	}

	public static void launchURL(WebDriver driver, String URL) {
		driver.get(URL);
		driver.manage().window().maximize();
		driver.navigate().refresh();
	}
}
